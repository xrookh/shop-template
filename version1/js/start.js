// Best offer sliders control
// let firstSlider = document.querySelector('.slider__item');
// let firstMutex = false;
// let secondSlider = document.querySelectorAll('.slider__item')[2];
// let secondMutex = false;
//
// firstSlider.addEventListener('click', handleSlider);
// secondSlider.addEventListener('click', handleSlider);
//
// function handleSlider(e) {
//     if (e.target.classList.contains('switch--up'))
//     {
//         let temp = getStockContainer(e);
//         if (temp !== undefined)
//         {
//             let mutexFlag;
//             let stock = temp.container;
//             mutexFlag = temp.flag;
//             let current = stock.querySelector('.item--active');
//             if(current.nextElementSibling === null)
//             {
//                 let stockNode = current.parentNode;
//                 let temp = stockNode.children[0].cloneNode(true);
//                 stock.style.transition = 'none';
//                 stock.style.bottom = parseInt(window.getComputedStyle(stock).bottom) - 452 + 'px';
//                 stockNode.removeChild(stockNode.children[0]);
//                 stockNode.appendChild(temp);
//                 current.classList.remove('item--active');
//                 current.nextElementSibling.classList.add('item--active');
//             } else {
//                 current.classList.remove('item--active');
//                 current.nextElementSibling.classList.add('item--active');
//             }
//
//             stock.style.bottom =  parseInt(window.getComputedStyle(stock).bottom) + 452 + 'px';
//             stock.style.transition = 'all .5s ease-in-out';
//
//             if (mutexFlag) setTimeout(unlockMutex, 500, true); else setTimeout(unlockMutex, 500, false);
//         }
//     }
//     else if (e.target.classList.contains('switch--down'))
//     {
//         let temp = getStockContainer(e);
//         if (temp !== undefined)
//         {
//             let mutexFlag = temp.flag;
//             let stock = temp.container;
//
//             let current = stock.querySelector('.item--active');
//             if(current.previousElementSibling === null)
//             {
//                 let stockNode = current.parentNode;
//                 let temp = stockNode.children[stockNode.children.length-1].cloneNode(true);
//                 stock.style.transition = 'none';
//                 stock.style.bottom = parseInt(window.getComputedStyle(stock).bottom) + 452 + 'px';
//                 stockNode.removeChild(stockNode.children[stockNode.children.length-1]);
//                 stockNode.prepend(temp);
//                 current.classList.remove('item--active');
//                 current.previousElementSibling.classList.add('item--active');
//             } else {
//                 current.classList.remove('item--active');
//                 current.previousElementSibling.classList.add('item--active');
//             }
//
//             stock.style.bottom =  parseInt(window.getComputedStyle(stock).bottom) - 452 + 'px';
//             stock.style.transition = 'all .5s ease-in-out';
//
//             if (mutexFlag) setTimeout(unlockMutex, 500, true); else setTimeout(unlockMutex, 500, false);
//         }
//     }
// }
//
// function unlockMutex(e) {
//     if (e) firstMutex = false;
//     else secondMutex = false;
// }
// function getStockContainer(e) {
//     if (e.target.classList.contains('first--item') && !firstMutex)
//     {
//         firstMutex = true;
//         return {container: firstSlider.querySelector('.stock__container'), flag:true};
//     }
//     else if (!e.target.classList.contains('first--item') && !secondMutex)
//     {
//         secondMutex = true;
//         return {container: secondSlider.querySelector('.stock__container'), flag:false};
//     }
// }
