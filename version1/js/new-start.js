// Handling mobile menu button
let dropDownSwitch = document.querySelector('.header__menu-icon');
dropDownSwitch.addEventListener('click', openMenu);

let dropDownMenu = document.querySelector('.header__menu');

function openMenu(e) {
    if (e.target.classList.contains('menu-icon')) {
        dropDownSwitch.classList.remove('header__menu-icon--passive');
        dropDownSwitch.classList.add('header__menu-icon--active');
        dropDownMenu.classList.add('header__menu--active');
        dropDownMenu.parentElement.classList.add('drop-shadow');
    } else if (e.target.classList.contains('close-icon')){
        dropDownSwitch.classList.remove('header__menu-icon--active');
        dropDownSwitch.classList.add('header__menu-icon--passive');
        dropDownMenu.classList.remove('header__menu--active');
        dropDownMenu.parentElement.classList.remove('drop-shadow');
    }
}

// Best offer sliders control
let firstSlider = document.querySelector('.main__slider-item');
let firstMutex = false;
let secondSlider = document.querySelectorAll('.main__slider-item')[1];
let secondMutex = false;

initBestOffer();

firstSlider.addEventListener('click', handleSlider);
secondSlider.addEventListener('click', handleSlider);

function handleSlider(e) {

    if (e.target.classList.contains('swipe-up'))
    {

        let temp = getStockContainer(e);
        if (temp !== undefined)
        {

            let mutexFlag;
            let stock = temp.container;
            mutexFlag = temp.flag;
            let current = stock.querySelector('.card--active');
            if(current.nextElementSibling === null)
            {
                let stockNode = current.parentNode;
                let temp = stockNode.children[0].cloneNode(true);
                stock.style.transition = 'none';
                stock.style.bottom =  parseInt(window.getComputedStyle(stock).bottom) - parseInt(window.getComputedStyle(stock.parentElement).height) + 'px';
                stockNode.removeChild(stockNode.children[0]);
                stockNode.appendChild(temp);
                current.classList.remove('card--active');
                current.nextElementSibling.classList.add('card--active');
            } else {
                current.classList.remove('card--active');
                current.nextElementSibling.classList.add('card--active');
            }

            stock.style.bottom = parseInt(window.getComputedStyle(stock).bottom) + parseInt(window.getComputedStyle(stock.parentElement).height) + 'px';
            stock.style.transition = 'all .5s ease-in-out';
            recalculateOffer();
            if (mutexFlag) setTimeout(unlockMutex, 500, true); else setTimeout(unlockMutex, 500, false);
        }
    }
    else if (e.target.classList.contains('swipe-down'))
    {
        let temp = getStockContainer(e);
        if (temp !== undefined)
        {
            let mutexFlag = temp.flag;
            let stock = temp.container;

            let current = stock.querySelector('.card--active');
            if(current.previousElementSibling === null)
            {
                let stockNode = current.parentNode;
                let temp = stockNode.children[stockNode.children.length-1].cloneNode(true);
                stock.style.transition = 'none';
                stock.style.bottom = parseInt(window.getComputedStyle(stock).bottom) + parseInt(window.getComputedStyle(stock.parentElement).height) + 'px';
                stockNode.removeChild(stockNode.children[stockNode.children.length-1]);
                stockNode.prepend(temp);
                current.classList.remove('card--active');
                current.previousElementSibling.classList.add('card--active');
            } else {
                current.classList.remove('card--active');
                current.previousElementSibling.classList.add('card--active');
            }

            stock.style.bottom =  parseInt(window.getComputedStyle(stock).bottom) - parseInt(window.getComputedStyle(stock.parentElement).height) + 'px';
            stock.style.transition = 'all .5s ease-in-out';
            recalculateOffer();
            if (mutexFlag) setTimeout(unlockMutex, 500, true); else setTimeout(unlockMutex, 500, false);
        }
    }
}

function unlockMutex(e) {
    if (e) firstMutex = false;
    else secondMutex = false;
}
function getStockContainer(e) {
    if ((e.target.classList.contains('left-item') || e.target.parentElement.classList.contains('left-item')) && !firstMutex)
    {
        firstMutex = true;
        return {container: firstSlider.querySelector('.stock__container'), flag:true};
    }
    else if ((e.target.classList.contains('right-item') || e.target.parentElement.classList.contains('right-item'))  && !secondMutex)
    {
        secondMutex = true;
        return {container: secondSlider.querySelector('.stock__container'), flag:false};
    }
}

function initBestOffer() {
    let firstLeft = true;
    let firstRight = true;
    let leftContainer = firstSlider.querySelector('.stock__container');
    let rightContainer = secondSlider.querySelector('.stock__container');


    for (let i=0; i<bestOffer.left.length; i++) {
        let id_index;
        let obj = catalog.find( (element, index, array) => {if (element.id===bestOffer.left[i]) {id_index = index;} return element.id === bestOffer.left[i]});
        let card = getCatalogCard(obj);
        if (firstLeft) {
            card.classList.add('card--active');
            firstLeft = false;
        }
        card.id = id_index + '_card';
        leftContainer.appendChild(card);
    }

    for (let i=0; i<bestOffer.right.length; i++) {
        let id_index;
        let obj = catalog.find( (element, index, array) => {if (element.id===bestOffer.right[i]) {id_index = index;} return element.id === bestOffer.right[i]});
        let card = getCatalogCard(obj);
        if (firstRight) {
            card.classList.add('card--active');
            firstRight = false;
        }
        card.id = id_index + '_card';
        rightContainer.appendChild(card);
    }

    recalculateOffer();
}

function getCatalogCard(param) {
    let card = document.createElement('div');
    card.classList.add('main__card');
    if (param.hasNew) {
        let temp = document.createElement('div');
        temp.textContent = 'NEW';
        temp.classList.add('main__card-badge');
        card.appendChild(temp);
    }
    let photoDiv = document.createElement("div");
    photoDiv.classList.add('main__card-photo');
    let photoImg = document.createElement("img");
    photoImg.classList.add('catalog-image');
    photoImg.src = param.thumbnail;
    photoDiv.appendChild(photoImg);

    let name = document.createElement('div');
    name.classList.add('main__card-name');
    name.textContent = param.title;
    let price = document.createElement('div');
    price.classList.add('main__card-price');
    price.textContent = '£' + param.discountedPrice.toFixed(2);

    card.appendChild(photoDiv);
    card.appendChild(name);
    card.appendChild(price);
    return card;
}

function recalculateOffer() {
    let items = document.querySelectorAll('.card--active');
    let catalogObjLeft = catalog[parseInt(items[0].id)];
    let catalogObjRight = catalog[parseInt(items[1].id)];

    let oldPrice = catalogObjLeft.price + catalogObjRight.price;
    document.querySelector('.summary__oldprice').textContent = '£' + oldPrice.toFixed(2);
    document.querySelector('.summary__newprice').textContent = '£' + (oldPrice - 15).toFixed(2);
}